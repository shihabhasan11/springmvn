FROM maven:3.6.3-jdk-8-slim AS build
COPY src /home/app/src

ARG NEXUS_USER
ARG NEXUS_PASSWORD
ARG NEXUS_MAVEN_PUBLIC_URL
ARG NEXUS_MAVEN_RELEASES_URL
ARG NEXUS_MAVEN_SNAPSHOTS_URL

ENV NEXUS_USER $NEXUS_USER
ENV NEXUS_PASSWORD $NEXUS_PASSWORD
ENV NEXUS_MAVEN_PUBLIC_URL $NEXUS_MAVEN_PUBLIC_URL
ENV NEXUS_MAVEN_RELEASES_URL $NEXUS_MAVEN_RELEASES_URL
ENV NEXUS_MAVEN_SNAPSHOTS_URL $NEXUS_MAVEN_SNAPSHOTS_URL

ADD script_to_set_settingfile.sh .
ADD script_to_set_pomfile.sh .
ADD settings.xml .
ADD pom.xml .
RUN /script_to_set_settingfile.sh
RUN /script_to_set_pomfile.sh
ADD settings.xml /root/.m2/settings.xml

COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package -DskipTests


FROM openjdk:8-jdk-alpine
COPY --from=build /home/app/target/*.jar /usr/local/lib/springmvn.jar
EXPOSE 8080
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/usr/local/lib/springmvn.jar"]
